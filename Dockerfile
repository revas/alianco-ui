FROM node:9.11.2-alpine as builder
ARG VUE_APP_BASE_DOMAIN
ARG VUE_APP_BASE_HOST
ARG VUE_APP_BASE_API_URL
ARG VUE_APP_AUTH_OAUTH2_DOMAIN
ARG VUE_APP_AUTH_OAUTH2_CLIENT_ID
ARG VUE_APP_AUTH_OAUTH2_REDIRECT_URI
ARG VUE_APP_AUTH_OAUTH2_AUDIENCE
ARG VUE_APP_VUE_INTERCOM_ID
ARG VUE_APP_SENTRY_DSN
WORKDIR /app
COPY package*.json ./
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh
RUN npm install
COPY . .
RUN npm run build

FROM nginx:stable
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /app/dist /usr/share/nginx/html
