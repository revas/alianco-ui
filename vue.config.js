module.exports = {
  lintOnSave: true,
  runtimeCompiler: true,
  devServer: {
    disableHostCheck: true,
    host: '0.0.0.0'
  }
}
