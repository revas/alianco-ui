import RevasUI from '@revas/ui/vue'

import aliancoStore from '@/modules/realms/realms.store'
import aliancoRoutes from '@/modules/realms/realms.router'

const routes = [].concat(aliancoRoutes)

const options = {
  base: {
    domain: process.env.VUE_APP_BASE_DOMAIN,
    host: process.env.VUE_APP_BASE_HOST,
    api: {
      url: process.env.VUE_APP_BASE_API_URL
    }
  },
  router: {
    default: {
      name: 'ALIANCO.CITIZEN_COLLECTION.$NAME',
      require: {
        realm: true
      }
    }
  },
  auth: {
    oauth2: {
      domain: process.env.VUE_APP_AUTH_OAUTH2_DOMAIN,
      clientID: process.env.VUE_APP_AUTH_OAUTH2_CLIENT_ID,
      redirectUri: process.env.VUE_APP_AUTH_OAUTH2_REDIRECT_URI,
      audience: process.env.VUE_APP_AUTH_OAUTH2_AUDIENCE
    }
  },
  platforms: {
    intercom: { id: process.env.VUE_APP_VUE_INTERCOM_ID },
    sentry: { dsn: process.env.VUE_APP_SENTRY_DSN }
  },
  mode: process.env.NODE_ENV
}
const config = RevasUI.configure(options)

const storeModules = {
  alianco: aliancoStore(config.http)
}

RevasUI.install('alianco', config, storeModules, routes)
