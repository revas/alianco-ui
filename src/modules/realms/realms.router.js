import VRealmEditor from '@/modules/realms/views/VRealmEditor'
import VCitizenCollection from '@/modules/realms/views/VCitizenCollection'

export default [
  {
    path: 'create',
    name: 'ALIANCO.REALM_EDITOR.$NAME',
    component: VRealmEditor
  },
  {
    path: 'contributors',
    name: 'ALIANCO.CITIZEN_COLLECTION.$NAME',
    component: VCitizenCollection,
    require: {
      realm: true
    }
  }
]
