export default {
  REALM_CREATE_REALM_REQUEST: 'REALM_CREATE_REALM_REQUEST',
  REALM_CREATE_REALM_SUCCESS: 'REALM_CREATE_REALM_SUCCESS',
  REALM_CREATE_REALM_FAILURE: 'REALM_CREATE_REALM_FAILURE',
  REALM_UPDATE_BILLING_ADDRESS_REQUEST: 'REALM_UPDATE_BILLING_ADDRESS_REQUEST',
  REALM_UPDATE_BILLING_ADDRESS_SUCCESS: 'REALM_UPDATE_BILLING_ADDRESS_SUCCESS',
  REALM_UPDATE_BILLING_ADDRESS_FAILURE: 'REALM_UPDATE_BILLING_ADDRESS_FAILURE',
  REALM_SEND_TOKEN_REQUEST: 'REALM_SEND_TOKEN_REQUEST',
  REALM_SEND_TOKEN_SUCCESS: 'REALM_SEND_TOKEN_SUCCESS',
  REALM_SEND_TOKEN_FAILURE: 'REALM_SEND_TOKEN_FAILURE'
}
