import types from './realms.store.mutations'

const createRealmsStore = ($http) => {
  return {
    namespaced: true,
    state: {
      isWaiting: false,
      realm: {},
      errors: []
    },
    getters: {
      isWaiting: (state) => {
        return state.isWaiting
      },
      errors: (state) => {
        return state.errors
      }
    },
    mutations: {
      [types.REALM_CREATE_REALM_REQUEST] (state) {
        state.isWaiting = true
      },
      [types.REALM_CREATE_REALM_SUCCESS] (state) {
        state.isWaiting = false
      },
      [types.REALM_CREATE_REALM_FAILURE] (state) {
        state.isWaiting = false
      }
    },
    actions: {
      async createRealm ({ commit, dispatch }, realm) {
        commit(types.REALM_CREATE_REALM_REQUEST)
        try {
          const response = await $http.post('/orgs/alianco.CreateRealms/', {
            profilesAliases: ['me'],
            realms: [realm]
          })
          commit(types.REALM_CREATE_REALM_SUCCESS, realm)
          dispatch('app/getRealms', null, { root: true })
          dispatch('app/changeCurrentRealm', response.data.realms[0], { root: true })
        } catch (e) {
          console.log(e)
          commit(types.REALM_CREATE_REALM_FAILURE)
        }
      }
    }
  }
}

export default createRealmsStore
